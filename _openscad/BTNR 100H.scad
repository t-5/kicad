// Block AVB 1,5-1-9 Sicherheitstransformator

A = 30; // width
B = 15; // depth
C = 10; // height
MP_offset = 6.5; // offset of mounting pins from front
PIN_offset_Y = 3; // offset of other pins to mounting pins
PIN_offset_X = 2.5; // offset of other pins to mounting pins
pinspacing = 2.5; // pin spacing
pindia_MP = 1.2;
pindia_PIN = 1.0;
pinlength = 3.5;

blue = [0, 0, 0.7];
silver= [0.7, 0.7, 0.7];

$fs = 0.2;

translate([-A/2, -B/2, 0]) {
    color(blue) {
        cube([A, B, C]);
    }
}
translate([0, -B/2, C/2]) {
    color(silver) {
        rotate([90, 0, 0]) {
            cylinder(r=2, h=40);
            cylinder(r=4, h=6);
        }
    }
}
translate([0, 0, -pinlength]) {
    translate([0, (-B/2)+MP_offset, 0]) {
        translate([-4.5 * PIN_offset_X, 0, 0]) {
            cylinder(r=pindia_MP/2, h=pinlength);
        }
        translate([+4.5 * PIN_offset_X, 0, 0]) {
            cylinder(r=pindia_MP/2, h=pinlength);
        }

        translate([0, PIN_offset_Y, 0]) {
            translate([-4.5 * PIN_offset_X, 0, 0]) {
                cylinder(r=pindia_PIN/2, h=pinlength);
            }
            translate([-1.5 * PIN_offset_X, 0, 0]) {
                cylinder(r=pindia_PIN/2, h=pinlength);
            }
            translate([+1.5 * PIN_offset_X, 0, 0]) {
                cylinder(r=pindia_PIN/2, h=pinlength);
            }
            translate([+4.5 * PIN_offset_X, 0, 0]) {
                cylinder(r=pindia_PIN/2, h=pinlength);
            }
        }

        translate([0, PIN_offset_Y*2, 0]) {
            translate([-5.5 * PIN_offset_X, 0, 0]) {
                cylinder(r=pindia_PIN/2, h=pinlength);
            }
            translate([-4.5 * PIN_offset_X, 0, 0]) {
                cylinder(r=pindia_PIN/2, h=pinlength);
            }
            translate([-3.5 * PIN_offset_X, 0, 0]) {
                cylinder(r=pindia_PIN/2, h=pinlength);
            }
            translate([-2.5 * PIN_offset_X, 0, 0]) {
                cylinder(r=pindia_PIN/2, h=pinlength);
            }
            translate([-1.5 * PIN_offset_X, 0, 0]) {
                cylinder(r=pindia_PIN/2, h=pinlength);
            }
            translate([-0.5 * PIN_offset_X, 0, 0]) {
                cylinder(r=pindia_PIN/2, h=pinlength);
            }
            translate([+0.5 * PIN_offset_X, 0, 0]) {
                cylinder(r=pindia_PIN/2, h=pinlength);
            }
            translate([+1.5 * PIN_offset_X, 0, 0]) {
                cylinder(r=pindia_PIN/2, h=pinlength);
            }
            translate([+2.5 * PIN_offset_X, 0, 0]) {
                cylinder(r=pindia_PIN/2, h=pinlength);
            }
            translate([+3.5 * PIN_offset_X, 0, 0]) {
                cylinder(r=pindia_PIN/2, h=pinlength);
            }
            translate([+4.5 * PIN_offset_X, 0, 0]) {
                cylinder(r=pindia_PIN/2, h=pinlength);
            }
            translate([+5.5 * PIN_offset_X, 0, 0]) {
                cylinder(r=pindia_PIN/2, h=pinlength);
            }
        }
    }
}