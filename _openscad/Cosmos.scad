// Block AVB 1,5-1-9 Sicherheitstransformator

A = 104; // width
B = 61; // depth
C = 36.7; // height

gray = [0.2, 0.2, 0.2];

$fs = 0.2;

translate([-A/2, -B/2, 0]) {
    color(gray) {
        cube([A, B, C]);
    }
}
