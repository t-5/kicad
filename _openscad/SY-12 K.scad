// SY-12 K Relais

A = 12.5; // width
B = 7.4; // depth
C = 9.5; // height

pindia = 0.5;
pinlength = 3.1;
pinraster = 2.54;

grey = [0.3, 0.4, 0.3];
silver= [0.7, 0.7, 0.7];

$fs = 0.2;

color(grey) {
    cube([A, B, C]);
}

color(silver) {
    translate([0, 0, -pinlength]) {
        for (b = [-1, 1]) {
            translate([0, (B / 2) + (b * pinraster), 0]) {
                for (a = [-2, -1, 2]) {
                    translate([(A / 2) + (a * pinraster) - 0.64, 0, 0])
                        cylinder(r=pindia / 2, h=pinlength);
                }
            }
        }
    }
}