// Block AVB 3,2-2-12 Sicherheitstransformator

A = 41; // width
B = 35; // depth
C = 30.8; // height
C_offset = 5; // inset/offset edges on top
pinspacing_A4 = 5; // individual pin spacing in A direction
pinspacing_B = 25; // pin spacing in B direction
pindia = 0.8;
pinlength = 5;

blue = [0, 0, 0.7];
silver= [0.7, 0.7, 0.7];

$fs = 0.2;

color(blue) {
    cube([A, B, C - C_offset]);
    translate([C_offset, C_offset, C - C_offset])
        cube([A - 2 * C_offset, B - 2 * C_offset, C_offset]);
}
color(silver) {
    translate([0, 0, -pinlength]) {
        translate([0, (B - pinspacing_B) / 2, 0]) {
            for(i = [0, 1, 3, 4]) {
                translate([(A - 4 * pinspacing_A4) / 2 + i * pinspacing_A4, 0, 0])
                    cylinder(r=pindia / 2, h=pinlength);
            }
        }
        translate([0, B - (B - pinspacing_B) / 2, 0]) {
            for(i = [0 : 4]) {
                translate([(A - 4 * pinspacing_A4) / 2 + i * pinspacing_A4, 0, 0])
                    cylinder(r=pindia / 2, h=pinlength);
            }
        }
    }
}