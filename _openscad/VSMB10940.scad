// SY-12 K Relais

A = 3; // width
B = 1; // depth
C = 0.4; // tabwidth

metal_thickness = 0.1;
overdraw = 0.001;

yellow = [0.8, 0.8, 0.0];
silver= [0.7, 0.7, 0.7];
white = [1.0, 1.0, 1.0];

$fs = 0.2;

color(white) {
    cube([A, B, B]);
}

color(yellow) {
    translate([1.5 * B, 0, overdraw]) {
        cylinder(B - 2 * overdraw, B - overdraw, B - overdraw);
    }
}

color(silver) {
    translate([-overdraw, B - metal_thickness + overdraw, -overdraw]) {
        cube([C + 2 * overdraw, metal_thickness, B + 2 * overdraw]);
    }
    translate([-overdraw, -overdraw, -overdraw]) {
        cube([C + 2 * overdraw, metal_thickness, B + 2 * overdraw]);
    }
    translate([-overdraw, overdraw, -overdraw]) {
        cube([metal_thickness + overdraw, B + 2 * overdraw, B + 2 * overdraw]);
    }
    translate([A , B, 0]) {
        rotate([0, 0, 180]) {
            translate([-overdraw, B - metal_thickness + overdraw, -overdraw]) {
                cube([C + 2 * overdraw, metal_thickness, B + 2 * overdraw]);
            }
            translate([-overdraw, -overdraw, -overdraw]) {
                cube([C + 2 * overdraw, metal_thickness, B + 2 * overdraw]);
            }
            translate([-overdraw, overdraw, -overdraw]) {
                cube([metal_thickness + overdraw, B + 2 * overdraw, B + 2 * overdraw]);
            }
        }
    }
}