// Block AVB 1,5-1-9 Sicherheitstransformator

A = 32.2; // width
B = 27.3; // depth
C = 23.8; // height
C_offset = 5; // inset/offset edges on top
pinspacing_A1 = 20; // pin spacing in A direction, primary (D)
pinspacing_A2 = 10; // pin spacing in A direction, primary (F)
pinspacing_B = 20; // pin spacing in B direction (E)
pindia = 0.8;
pinlength = 5;

blue = [0, 0, 0.7];
silver= [0.7, 0.7, 0.7];

$fs = 0.2;

color(blue) {
    cube([A, B, C - C_offset]);
    translate([C_offset, C_offset, C - C_offset])
        cube([A - 2 * C_offset, B - 2 * C_offset, C_offset]);
}
color(silver) {
    translate([0, 0, -pinlength]) {
        translate([0, (B - pinspacing_B) / 2, 0]) {
            for(i = [0, 1]) {
                translate([(A - pinspacing_A1) / 2 + i * pinspacing_A1, 0, 0])
                    cylinder(r=pindia / 2, h=pinlength);
            }
        }
        translate([0, B - (B - pinspacing_B) / 2, 0]) {
            for(i = [0, 1]) {
                translate([(A - pinspacing_A2) / 2 + i * pinspacing_A2, 0, 0])
                    cylinder(r=pindia / 2, h=pinlength);
            }
        }
    }
}