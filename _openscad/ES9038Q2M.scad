// SY-12 K Relais

A = 3; // width
B = 5; // depth
C = 0.7; // height

grey = [0.3, 0.4, 0.3];

$fs = 0.2;

color(grey) {
    cube([A, B, C]);
}
